package org.sylrsykssoft.musbands;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication()
@ComponentScan("org.sylrsykssoft.musbands")
@EnableJpaRepositories("org.sylrsykssoft.musbands.repository")
@EntityScan("org.sylrsykssoft.musbands.domain")
public class AppRepository {

	
	public static void main(String[] args) {
		SpringApplication.run(AppRepository.class, args);
	}
}
