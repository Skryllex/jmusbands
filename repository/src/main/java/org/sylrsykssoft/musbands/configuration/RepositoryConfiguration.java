/**
 * 
 */
package org.sylrsykssoft.musbands.configuration;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@SpringBootConfiguration()
@EnableAutoConfiguration()
@EnableJpaRepositories("org.sylrsykssoft.musbands.repository")
@EntityScan("org.sylrsykssoft.musbands.domain")
public final class RepositoryConfiguration {

}
