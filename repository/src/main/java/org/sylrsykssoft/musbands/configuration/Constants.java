package org.sylrsykssoft.musbands.configuration;

public final class Constants {

	public static final String ALBUM_ENTITY_NAME = "album";
	public static final String ALBUM_ENTITY_COLLECTIONS_NAME = "albums";
	
	public static final String FUNCTION_ENTITY_NAME = "function";
	public static final String FUNCTION_ENTITY_COLLECTIONS_NAME = "functions";
	public static final String MUSICALGENRE_ENTITY_NAME = "musical_genre";
	public static final String MUSICALGENRE_ENTITY_COLLECTIONS_NAME = "musical_genres";
	public static final String STATEMEMBER_ENTITY_NAME = "state_member";
	public static final String STATEMEMBER_ENTITY_COLLECTIONS_NAME = "state_members";
	
	private Constants() {

	}
}
