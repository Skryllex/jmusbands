package org.sylrsykssoft.musbands.domain;

public enum Gender {
	MALE,
	FEMALE,
	UNKNOWN
}
