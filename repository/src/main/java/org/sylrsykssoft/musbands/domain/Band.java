package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "band")
@Data()
public final class Band extends Base implements Serializable {

	private static final long serialVersionUID = 8664851625066306865L;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "biography", nullable = true)
	private String biography;

	@Column(name = "year_beginning", nullable = false)
	private String yearBeginning;

	@Column(name = "year_end", nullable = true)
	private String yearEnd;

	@Column(name = "website", nullable = true)
	private String website;

	@ManyToMany(targetEntity = MusicalGenre.class, cascade = CascadeType.ALL)
	private List<MusicalGenre> musicalGenres;

	@ManyToMany(targetEntity = Label.class, cascade = CascadeType.ALL)
	private List<Label> labels;

	@ManyToMany(targetEntity = Member.class, cascade = CascadeType.ALL)
	private List<Member> members;

	@OneToMany(targetEntity = Album.class, mappedBy = "band", cascade = CascadeType.ALL)
	private List<Album> albums;

	@OneToMany(targetEntity = Concert.class, mappedBy = "band", cascade = CascadeType.ALL)
	private List<Concert> concerts;
	
	/**
	 * Add album.
	 * 
	 * @param album Album
	 * @return Band
	 */
	public Band addAlbum(final Album album) {
		this.albums.add(album);
		return this;
	}
	
	/**
	 * Add albums.
	 * 
	 * @param albums
	 * @return Band
	 */
	public Band addAlbums(final List<Album> albums) {
		this.albums.addAll(albums);
		return this;
	}
	
	/**
	 * Add concert.
	 * 
	 * @param concert Concert
	 * @return Band
	 */
	public Band addConcert(final Concert concert) {
		this.concerts.add(concert);
		return this;
	}
	
	/**
	 * Add concerts.
	 * 
	 * @param concerts List of concerts.
	 * @return Band
	 */
	public Band addConcerts(final List<Concert> concerts) {
		this.concerts.addAll(concerts);
		return this;
	}

}
