package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "label")
@Data()
public final class Label extends Base implements Serializable {

	private static final long serialVersionUID = 8320962366811215940L;

	@Column(name = "name", nullable = false, unique = true, length = 256)
	private String name;

	@Column(name = "description", nullable = true, length = 1000)
	private String description;

	@Column(name = "website", nullable = true, length = 256)
	private String website;

	@ManyToMany(targetEntity = Band.class, mappedBy = "labels", fetch = FetchType.EAGER)
	private List<Band> bands;

}
