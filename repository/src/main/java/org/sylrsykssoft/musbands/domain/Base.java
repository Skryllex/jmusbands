/**
 * 
 */
package org.sylrsykssoft.musbands.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import org.springframework.hateoas.Identifiable;

import lombok.Data;

/**
 * Entity base.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@MappedSuperclass()
@Data()
public abstract class Base {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	protected Long id;

	@Column(name = "created_at", nullable = false, insertable = true, updatable = false)
	protected Date createdAt;

	@Column(name = "updated_at", nullable = true, insertable = false, updatable = true)
	protected Date updatedAt;

	@Column(name = "deleted_at", nullable = true)
	protected Date deletedAt;
}
