package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "musical_genre")
@Data()
public final class MusicalGenre extends BaseAdmin implements Serializable {

	private static final long serialVersionUID = -6333973991470303615L;
	
	@ManyToMany(targetEntity = Band.class, mappedBy = "musicalGenres", fetch = FetchType.EAGER)
	private List<Band> bands;

}
