package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "album")
@Data()
public final class Album extends Base implements Serializable {

	private static final long serialVersionUID = 2784047528498396814L;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "year", nullable = false)
	private String year;

	@ManyToOne(targetEntity = Band.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_band")
	private Band band;

	@OneToMany(targetEntity = Song.class, mappedBy = "album", cascade = CascadeType.ALL)
	private List<Song> songs;

	/**
	 * Add song.
	 * 
	 * @param song Object song
	 * @return Album
	 */
	public Album addSong(final Song song) {
		this.songs.add(song);
		return this;
	}
	
	/**
	 * Add songs.
	 * 
	 * @param songs List of songs
	 * @return Album
	 * 
	 */
	public Album addSongs(final List<Song> songs) {
		this.songs.addAll(songs);
		return this;
	}
}
