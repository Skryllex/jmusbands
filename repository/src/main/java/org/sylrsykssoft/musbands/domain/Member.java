package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "member")
@Data()
public final class Member extends Base implements Serializable {

	private static final long serialVersionUID = -6051112693814338878L;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "surname", nullable = true)
	private String surname;

	@Column(name = "age", nullable = true)
	private String age;

	@Column(name = "website", nullable = true)
	private String website;

	@OneToMany(targetEntity = Function.class, mappedBy = "member", cascade = CascadeType.ALL)
	private List<Function> functions;

	@OneToMany(targetEntity = StateMember.class, mappedBy = "member", cascade = CascadeType.ALL)
	private List<StateMember> states;

	@ManyToMany(targetEntity = Band.class, mappedBy = "albums", cascade = CascadeType.ALL)
	private List<Band> bands;

	/**
	 * Add function.
	 * 
	 * @param function
	 *            Function.
	 * @return Member
	 */
	public Member addFunction(final Function function) {
		this.functions.add(function);
		return this;
	}

	/**
	 * Add functions.
	 * 
	 * @param functions
	 *            List of functions.
	 * @return Member
	 */
	public Member addFunctions(final List<Function> functions) {
		this.functions.addAll(functions);
		return this;
	}

	/**
	 * Add state.
	 * 
	 * @param state
	 *            StateMember.
	 * @return Member
	 */
	public Member addState(final StateMember state) {
		this.states.add(state);
		return this;
	}

	/**
	 * Add states.
	 * 
	 * @param states
	 *            List of state members.
	 * @return Member
	 */
	public Member addStates(final List<StateMember> states) {
		this.states.addAll(states);
		return this;
	}

}
