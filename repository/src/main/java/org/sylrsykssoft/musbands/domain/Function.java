package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "function")
@Data()
public final class Function extends BaseAdmin implements Serializable {

	private static final long serialVersionUID = 3233380885357967219L;

	@ManyToOne(targetEntity = Member.class, optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_member")
	private Member member;

}
