package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "state_member")
@Data()
public final class StateMember extends BaseAdmin implements Serializable {

	private static final long serialVersionUID = -1284784957545987335L;
	
	@ManyToOne(targetEntity = Member.class, optional = true, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_member")
	private Member member;

}
