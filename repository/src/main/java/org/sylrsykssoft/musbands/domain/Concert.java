package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import lombok.Data;

@Entity()
@Table(name = "concert", uniqueConstraints = @UniqueConstraint(columnNames = { "date", "location" }))
@Data()
public final class Concert extends Base implements Serializable {

	private static final long serialVersionUID = 9193931543537057494L;

	@Column(name = "date", nullable = false)
	private Date date;

	@Column(name = "location", nullable = false)
	private String location;

	@ManyToOne(targetEntity = Band.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_band")
	private Band band;

}
