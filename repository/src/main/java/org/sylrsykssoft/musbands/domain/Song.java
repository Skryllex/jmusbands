package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "song")
@Data()
public final class Song extends Base implements Serializable {

	private static final long serialVersionUID = 6139216954797030208L;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "duration", nullable = false)
	private String duration;
	
	@ManyToOne(targetEntity = Album.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "id_album")
	private Album album;

}
