/**
 * 
 */
package org.sylrsykssoft.musbands.domain;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import lombok.Data;

@Entity()
@Table(name = "user")
@Data()
public final class User extends Base implements Serializable {

	private static final long serialVersionUID = 7815654993267397008L;

	@Column(name = "user_name", nullable = false)
	private String username;

	@Column(name = "name", nullable = false)
	private String name;

	@Column(name = "surname", nullable = true)
	private String surname;

	@Column(name = "email", nullable = false)
	private String email;

	@Column(name = "gender", nullable = true)
	private Gender gender;

	@Column(name = "country", nullable = true)
	private String country;

	@Column(name = "postal_code", nullable = true)
	private String postalCode;

	@Column(name = "date_of_birth", nullable = true)
	private Date dateOfBirth;

}
