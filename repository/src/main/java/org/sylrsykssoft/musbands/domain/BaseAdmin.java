package org.sylrsykssoft.musbands.domain;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import lombok.Data;

@MappedSuperclass()
@Data()
public abstract class BaseAdmin extends Base {

	@Column(name = "name", nullable = false, unique = true, length = 256)
	private String name;

	@Column(name = "descriptiom", nullable = true, length = 1000)
	private String description;
}
