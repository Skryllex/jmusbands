package org.sylrsykssoft.musbands.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.sylrsykssoft.musbands.configuration.Constants;
import org.sylrsykssoft.musbands.domain.StateMember;
import org.sylrsykssoft.musbands.repository.query.RepositoryQuery;

/**
 * StateMember repository.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@RepositoryRestResource(collectionResourceRel = Constants.STATEMEMBER_ENTITY_NAME, path = Constants.STATEMEMBER_ENTITY_NAME)
public interface StateMemberRepository extends AdminRepository<StateMember> {

	@Query(RepositoryQuery.STATEMEMBER_FIND_BY_NAME)
	@Override()
	StateMember findByName(@Param("name") final String name);
}
