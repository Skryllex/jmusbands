package org.sylrsykssoft.musbands.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.sylrsykssoft.musbands.configuration.Constants;
import org.sylrsykssoft.musbands.domain.Function;
import org.sylrsykssoft.musbands.repository.query.RepositoryQuery;

/**
 * Function repository.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@RepositoryRestResource(collectionResourceRel = Constants.FUNCTION_ENTITY_NAME, path = Constants.FUNCTION_ENTITY_NAME)
public interface FunctionRepository extends AdminRepository<Function> {

	/**
	 * Find function by name.
	 */
	@Query(RepositoryQuery.FUNCTION_FIND_BY_NAME)
	@Override()
	Function findByName(@Param("name") final String name);
}
