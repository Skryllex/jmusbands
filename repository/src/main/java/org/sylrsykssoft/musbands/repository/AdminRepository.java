package org.sylrsykssoft.musbands.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.sylrsykssoft.musbands.domain.BaseAdmin;

@NoRepositoryBean()
public interface AdminRepository<T extends BaseAdmin> extends CrudRepository<T, Long> {
	
	/**
	 * Find by name.
	 * 
	 * @param name Value of the attribute name
	 * 
	 * @return T entity.
	 */
	T findByName(final String name);
}
