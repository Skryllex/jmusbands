package org.sylrsykssoft.musbands.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.sylrsykssoft.musbands.configuration.Constants;
import org.sylrsykssoft.musbands.domain.Album;

@RepositoryRestResource(collectionResourceRel = Constants.ALBUM_ENTITY_NAME, path = "album")
public interface AlbumRepository extends Repository<Album> {

	@Query("SELECT a FROM #{#entityName} WHERE a.name = :name")
	List<Album> findByName(@Param("name") final String name);
}
