package org.sylrsykssoft.musbands.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;
import org.sylrsykssoft.musbands.domain.Base;

@NoRepositoryBean
public interface Repository<T extends Base> extends CrudRepository<T, Long> {

}
