package org.sylrsykssoft.musbands.repository.query;

public final class RepositoryQuery {

	// Tables.
	public static final String FUNCTION_ENTITY_REPOSITORY_NAME = "Function";
	public static final String MUSICALGENRE_ENTITY_REPOSITORY_NAME = "MusicalGenre";
	public static final String STATEMEMBER_ENTITY_REPOSITORY_NAME = "StateMember";

	// Queries
	public static final String FUNCTION_FIND_BY_NAME = "SELECT t FROM " + FUNCTION_ENTITY_REPOSITORY_NAME
			+ " t WHERE t.name = :name";
	public static final String MUSICALGENRE_FIND_BY_NAME = "SELECT t FROM " + MUSICALGENRE_ENTITY_REPOSITORY_NAME
			+ " t WHERE t.name = :name";
	public static final String STATEMEMBER_FIND_BY_NAME = "SELECT t FROM " + STATEMEMBER_ENTITY_REPOSITORY_NAME
			+ " t WHERE t.name = :name";

	private RepositoryQuery() {

	}
}
