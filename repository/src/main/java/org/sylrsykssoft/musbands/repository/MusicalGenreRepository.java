package org.sylrsykssoft.musbands.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import org.sylrsykssoft.musbands.configuration.Constants;
import org.sylrsykssoft.musbands.domain.MusicalGenre;
import org.sylrsykssoft.musbands.repository.query.RepositoryQuery;

/**
 * MusicalGenre repository.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@RepositoryRestResource(collectionResourceRel = Constants.MUSICALGENRE_ENTITY_NAME, path = Constants.MUSICALGENRE_ENTITY_NAME)
public interface MusicalGenreRepository extends AdminRepository<MusicalGenre> {

	@Query(RepositoryQuery.MUSICALGENRE_FIND_BY_NAME)
	@Override()
	MusicalGenre findByName(@Param("name") final String name);
}
