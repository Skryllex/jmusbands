package org.sylrsykssoft.musbands;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.sylrsykssoft.musbands.configuration.Routes;
import org.sylrsykssoft.musbands.domain.Album;
import org.sylrsykssoft.musbands.exception.NotFoundEntityException;
import org.sylrsykssoft.musbands.service.AlbumService;

import lombok.extern.slf4j.Slf4j;

@RestController("albumController")
@RequestMapping(path = Routes.ENTITY_ALBUM_COLLECTION)
@Slf4j()
public final class AlbumController extends BaseController<Album> {

	@Autowired()
	private AlbumService albumService;

	/**
	 * Get albums by name.
	 * 
	 * @param name Albun name
	 * @return List<Album>
	 * @throws NotFoundEntityException
	 */
	@GetMapping(path = "/album/{name}")
	public List<Album> findByName(@PathVariable final String name) throws NotFoundEntityException {
		LOGGER.info("AlbumController:findByName Find album by name {}", name);
		
		final List<Album> albums = albumService.findByName(name);
		if (albums == null) {
			throw new NotFoundEntityException();
		}
		
		LOGGER.info("AlbumController:findByName Found {} album entries.", albums.size());
		
		return albums;
	}

}
