package org.sylrsykssoft.musbands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.sylrsykssoft.musbands.configuration.Routes;
import org.sylrsykssoft.musbands.domain.Base;
import org.sylrsykssoft.musbands.exception.AppException;
import org.sylrsykssoft.musbands.exception.NotFoundEntityException;
import org.sylrsykssoft.musbands.exception.NotIdMismatchEntityException;
import org.sylrsykssoft.musbands.service.BaseService;

import lombok.extern.slf4j.Slf4j;

@Slf4j()
public class BaseController<T extends Base> {

	@Autowired()
	protected BaseService<T> service;

	/**
	 * Find all entries.
	 * 
	 * @return Iterable<T> entries.
	 * 
	 * @throws NotFoundEntityException
	 */
	@GetMapping(Routes.ENTITY_FIND_ALL)
	public Iterable<T> findAll() throws NotFoundEntityException {
		LOGGER.info("BaseController:findAll Finding all entries");

		final Iterable<T> entities = service.findAll();
		if (entities == null) {
			throw new NotFoundEntityException();
		}

		LOGGER.info("BaseController:findAll Found {} entries.", entities);

		return entities;
	}

	/**
	 * Find one entry.
	 * 
	 * @param id
	 *            Id
	 * 
	 * @return T entry.
	 * 
	 * @throws NotFoundEntityException
	 */
	@GetMapping(path = Routes.ENTITY_FIND_ONE)
	public T findOne(@PathVariable final Long id) throws NotFoundEntityException {
		LOGGER.info("BaseController:findOne Find entry with id {}", id);
		final T entity = service.findOne(id);

		if (entity == null) {
			throw new NotFoundEntityException();
		}

		LOGGER.info("BaseController:findOne Found {} entry.", entity);
		return entity;
	}

	/**
	 * Create entry.
	 * 
	 * @param entity
	 *            Entity.
	 * 
	 * @return T entity.
	 */
	@PutMapping(path = Routes.ENTITY_CREATE)
	@ResponseStatus(code = HttpStatus.CREATED)
	public T create(@RequestBody final T entity) {
		LOGGER.info("BaseController:create Creating a new todo entry by using information: {}", entity);

		final T created = service.save(entity);

		LOGGER.info("BaseController:create Created a new todo entry: {}", created);

		return created;
	}

	/**
	 * Update entity.
	 * 
	 * @param entity
	 *            Entity.
	 * @param id
	 *            Id.
	 * 
	 * @return T entity.
	 * 
	 * @throws NotIdMismatchEntityException
	 * @throws NotFoundEntityException
	 */
	@PutMapping(path = Routes.ENTITY_UPDATE)
	public T update(@RequestBody final T entity, @PathVariable final Long id)
			throws NotIdMismatchEntityException, NotFoundEntityException {
		LOGGER.info("BaseController:update Updating a entry with id: {}", id);

		if (entity.getId() != id) {
			throw new NotIdMismatchEntityException();
		}

		final T old = service.findOne(id);
		if (old == null) {
			throw new NotFoundEntityException();
		}

		final T updated = service.save(entity);

		LOGGER.info("BaseController:update Updated the entry: {}", updated);

		return updated;
	}

	/**
	 * Delete entry.
	 * 
	 * @param id
	 *            Id.
	 * 
	 * @throws NotFoundEntityException
	 * @throws AppException
	 */
	@DeleteMapping(path = Routes.ENTITY_DELETE)
	public void delete(@PathVariable final Long id) throws NotFoundEntityException, AppException {
		LOGGER.info("BaseController:delete Deleting a entry with id: {}", id);

		final T old = service.findOne(id);
		if (old == null) {
			throw new NotFoundEntityException();
		}

		try {
			service.delete(id);
		} catch (Exception e) {
			throw new AppException();
		}
	}

}
