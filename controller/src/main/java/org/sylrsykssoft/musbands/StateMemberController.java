package org.sylrsykssoft.musbands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.sylrsykssoft.musbands.configuration.Routes;
import org.sylrsykssoft.musbands.domain.StateMember;
import org.sylrsykssoft.musbands.service.StateMemberService;

import lombok.extern.slf4j.Slf4j;

/**
 * StateMember controller.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@RestController("stateMemberController")
@RequestMapping(path = Routes.ADMIN_ENTITY_STATE_MEMBER_COLLECTION)
@Slf4j()
public final class StateMemberController extends BaseAdminController<StateMember> {
	
	@Autowired()
	protected StateMemberService stateMemberService;

}
