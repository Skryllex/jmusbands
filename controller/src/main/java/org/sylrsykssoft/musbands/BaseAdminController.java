package org.sylrsykssoft.musbands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.sylrsykssoft.musbands.configuration.Routes;
import org.sylrsykssoft.musbands.domain.BaseAdmin;
import org.sylrsykssoft.musbands.exception.NotFoundEntityException;
import org.sylrsykssoft.musbands.service.BaseAdminService;

import lombok.extern.slf4j.Slf4j;

@Slf4j()
public class BaseAdminController<T extends BaseAdmin> extends BaseController<T> {

	@Autowired()
	protected BaseAdminService<T> adminService;

	/**
	 * Find by name.
	 * 
	 * @param name Value of attribute name.
	 * 
	 * @return T entity.
	 * 
	 * @throws NotFoundEntityException
	 */
	@GetMapping(path = Routes.ADMIN_ENTITY_FIND_BY_NAME)
	public T findByName(@PathVariable final String name) throws NotFoundEntityException {
		LOGGER.info("BaseAdminController:findByName name {}", name);

		final T entity = adminService.findByName(name);
		if (entity == null) {
			throw new NotFoundEntityException();
		}

		LOGGER.info("BaseAdminController:findByName Found {}", entity);

		return entity;
	}

}
