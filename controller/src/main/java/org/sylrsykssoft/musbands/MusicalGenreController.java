package org.sylrsykssoft.musbands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.sylrsykssoft.musbands.configuration.Routes;
import org.sylrsykssoft.musbands.domain.MusicalGenre;
import org.sylrsykssoft.musbands.service.MusicalGenreService;

import lombok.extern.slf4j.Slf4j;

/**
 * MusicalGenre controller.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@RestController("musicalGenreController")
@RequestMapping(path = Routes.ADMIN_ENTITY_MUSICAL_GENRE_COLLECTION)
@Slf4j()
public final class MusicalGenreController extends BaseAdminController<MusicalGenre> {
	
	@Autowired()
	protected MusicalGenreService genreService;

}
