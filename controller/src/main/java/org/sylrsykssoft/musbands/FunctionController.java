package org.sylrsykssoft.musbands;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.sylrsykssoft.musbands.configuration.Routes;
import org.sylrsykssoft.musbands.domain.MusicalGenre;
import org.sylrsykssoft.musbands.service.FunctionService;

import lombok.extern.slf4j.Slf4j;

/**
 * Function controller.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@RestController("functionController")
@RequestMapping(path = Routes.ADMIN_ENTITY_FUNCTION_COLLECTION)
@Slf4j()
public final class FunctionController extends BaseAdminController<MusicalGenre> {
	
	@Autowired()
	protected FunctionService functionService;

}
