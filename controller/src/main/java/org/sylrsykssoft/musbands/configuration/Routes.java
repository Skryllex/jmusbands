package org.sylrsykssoft.musbands.configuration;

public final class Routes {

	// Common routes.
	public static final String API_ROOT = "/api/";
	public static final String API_ADMIN_ROOT = "/api/admin/";
	public static final String ENTITY_FIND_ALL = "";
	public static final String ENTITY_FIND_ONE = "/{id}";
	public static final String ENTITY_CREATE = "";
	public static final String ENTITY_UPDATE = "/{id}";
	public static final String ENTITY_DELETE = "/{id}";
	public static final String ADMIN_ENTITY_FIND_BY_NAME = "/{name}";

	// Entities.
	public static final String ENTITY_ALBUM = API_ROOT + Constants.ALBUM_ENTITY_NAME;
	public static final String ENTITY_ALBUM_COLLECTION = API_ROOT + Constants.ALBUM_ENTITY_COLLECTIONS_NAME;

	// Admin Entities.
	public static final String ADMIN_ENTITY_FUNCTION = API_ADMIN_ROOT + Constants.FUNCTION_ENTITY_NAME;
	public static final String ADMIN_ENTITY_FUNCTION_COLLECTION = API_ADMIN_ROOT
			+ Constants.FUNCTION_ENTITY_COLLECTIONS_NAME;
	public static final String ADMIN_ENTITY_MUSICAL_GENRE = API_ADMIN_ROOT + Constants.MUSICALGENRE_ENTITY_NAME;
	public static final String ADMIN_ENTITY_MUSICAL_GENRE_COLLECTION = API_ADMIN_ROOT
			+ Constants.MUSICALGENRE_ENTITY_COLLECTIONS_NAME;
	public static final String ADMIN_ENTITY_STATE_MEMBER = API_ADMIN_ROOT + Constants.STATEMEMBER_ENTITY_NAME;
	public static final String ADMIN_ENTITY_STATE_MEMBER_COLLECTION = API_ADMIN_ROOT
			+ Constants.STATEMEMBER_ENTITY_COLLECTIONS_NAME;

	private Routes() {

	}
}
