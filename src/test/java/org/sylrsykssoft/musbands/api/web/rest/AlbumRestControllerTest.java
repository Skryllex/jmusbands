package org.sylrsykssoft.musbands.api.web.rest;

import static io.restassured.RestAssured.preemptive;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.sylrsykssoft.musbands.App;
import org.sylrsykssoft.musbands.api.model.domain.Album;

import io.restassured.RestAssured;
import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { App.class }, webEnvironment = WebEnvironment.DEFINED_PORT)
public final class AlbumRestControllerTest {

	private static final String API_ROOT = "http://localhost:8081/api/albums";
	
	@Before
    public void setUp() {
        RestAssured.authentication = preemptive().basic("john doe", "123");
    }
	
	@Test
    public void whenGetAllAlbums_thenOK() {
        final Response response = RestAssured.get(API_ROOT);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }
	
    public void whenGetAlbumsByName_thenOK() {
        final Album album = createRandomAlbum();
        createAlbumAsUri(album);

        final Response response = RestAssured.get(API_ROOT + "/album/" + album.getName());
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(response.as(List.class).size() > 0);
    }
	
	private Album createRandomAlbum() {
        final Album album = new Album();
        album.setName("The Bedroom Sessions");
        return album;
    }

    private String createAlbumAsUri(Album book) {
        final Response response = RestAssured.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(book)
            .post(API_ROOT);
        return API_ROOT + "/" + response.jsonPath()
            .get("id");
    }
}
