package org.sylrsykssoft.musbands.api.web.rest;

import static io.restassured.RestAssured.preemptive;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.sylrsykssoft.musbands.App;
import org.sylrsykssoft.musbands.api.model.domain.MusicalGenre;

import io.restassured.RestAssured;
import io.restassured.response.Response;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { App.class }, webEnvironment = WebEnvironment.DEFINED_PORT)
public final class MusicalGenreRestControllerTest {

	private static final String API_ROOT = "http://localhost:8081/api/musical_genre";
	
	@Before
    public void setUp() {
        RestAssured.authentication = preemptive().basic("john doe", "123");
    }
	
	@Test
    public void whenGetAllMusicalGenre_thenOK() {
        final Response response = RestAssured.get(API_ROOT);
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
    }
	
	@Test
    public void whenGetMusicalGenreByName_thenOK() {
        final MusicalGenre genre = createRandomMusicalGenre();
        createMusicalGenreAsUri(genre);

        final Response response = RestAssured.get(API_ROOT + "/" + genre.getName());
        assertEquals(HttpStatus.OK.value(), response.getStatusCode());
        assertTrue(response.as(List.class).size() > 0);
    }
	
	private MusicalGenre createRandomMusicalGenre() {
        final MusicalGenre genre = new MusicalGenre();
        genre.setName("Alternative metal");
        genre.setDescription("Alternative metal (also known as alt-metal) is a rock music fusion genre that infuses heavy metal with influences from alternative rock and other genres not normally associated with metal.");
        return genre;
    }

    private String createMusicalGenreAsUri(MusicalGenre genre) {
        final Response response = RestAssured.given()
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .body(genre)
            .post(API_ROOT);
        return API_ROOT + "/" + response.jsonPath()
            .get("id");
    }
}
