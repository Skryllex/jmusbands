/**
 * 
 */
package org.sylrsykssoft.musbands.configuration;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@SpringBootConfiguration()
@EnableAutoConfiguration()
@ComponentScan("org.sylrsykssoft.musbands.service")
public final class ServiceConfiguration {

}
