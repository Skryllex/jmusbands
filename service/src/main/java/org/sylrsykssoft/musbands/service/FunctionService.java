package org.sylrsykssoft.musbands.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sylrsykssoft.musbands.domain.Function;
import org.sylrsykssoft.musbands.repository.FunctionRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * Function service.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@Service("functionService")
@Transactional()
@Slf4j()
public final class FunctionService extends BaseAdminService<Function> {

	@Autowired()
	private FunctionRepository functionRepository;
}
