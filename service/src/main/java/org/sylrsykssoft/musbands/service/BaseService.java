package org.sylrsykssoft.musbands.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.sylrsykssoft.musbands.domain.Base;
import org.sylrsykssoft.musbands.repository.Repository;

import lombok.extern.slf4j.Slf4j;

/**
 * BaseService service.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 * @param <T> Type class.
 */
@Slf4j()
public class BaseService<T extends Base> implements Service<T, Long> {

	@Autowired()
	private Repository<T> repository;

	@Override()
	public <S extends T> S save(S entity) {
		LOGGER.info("BaseService:save Save entity {}.", entity);
		return repository.save(entity);
	}

	@Override()
	public <S extends T> Iterable<S> save(Iterable<S> entities) {
		LOGGER.info("BaseService:save Save entities {}.", entities);
		return repository.save(entities);
	}

	@Override()
	public T findOne(Long id) {
		LOGGER.info("BaseService:findOne Find one by id {}.", id);
		return repository.findOne(id);
	}

	@Override()
	public boolean exists(Long id) {
		LOGGER.info("BaseService:exists Exists entity by id {}.", id);
		return repository.exists(id);
	}

	@Override
	public Iterable<T> findAll() {
		LOGGER.info("BaseService:findAll Find all.");
		return repository.findAll();
	}

	@Override()
	public Iterable<T> findAll(Iterable<Long> ids) {
		LOGGER.info("BaseService:findAll Find all by ids {}.", ids);
		return repository.findAll(ids);
	}

	@Override()
	public long count() {
		LOGGER.info("BaseService:count Count.");
		return repository.count();
	}

	@Override()
	public void delete(Long id) {
		LOGGER.info("BaseService:delete Delete entity by id {}.", id);
		repository.delete(id);
	}

	@Override()
	public void delete(T entity) {
		LOGGER.info("BaseService:delete Delete by entity {}.", entity);
		repository.delete(entity);
	}

	@Override()
	public void delete(Iterable<? extends T> entities) {
		LOGGER.info("BaseService:delete Delete entities by list {}.", entities);
		repository.delete(entities);
	}

	@Override()
	public void deleteAll() {
		LOGGER.info("BaseService:deleteAll Delete all entities.");
		repository.deleteAll();
	}

}
