package org.sylrsykssoft.musbands.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sylrsykssoft.musbands.domain.StateMember;
import org.sylrsykssoft.musbands.repository.StateMemberRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * StateMember service.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@Service("stateMemberService")
@Transactional()
@Slf4j()
public final class StateMemberService extends BaseAdminService<StateMember> {

	@Autowired()
	private StateMemberRepository stateMemberRepository;
}
