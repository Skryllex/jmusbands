package org.sylrsykssoft.musbands.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.sylrsykssoft.musbands.domain.BaseAdmin;
import org.sylrsykssoft.musbands.repository.AdminRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * BaseAdminService service.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 * @param <T> Type class.
 */
@Slf4j()
public class BaseAdminService<T extends BaseAdmin> extends BaseService<T> implements AdminService<T> {

	@Autowired()
	private AdminRepository<T> adminRepository;
	
	@Override()
	public T findByName(String name) {
		LOGGER.info("BaseAdminService:findByName Find by name {}.", name);
		return adminRepository.findByName(name);
	}

}
