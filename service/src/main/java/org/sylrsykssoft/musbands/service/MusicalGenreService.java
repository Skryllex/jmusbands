package org.sylrsykssoft.musbands.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sylrsykssoft.musbands.domain.MusicalGenre;
import org.sylrsykssoft.musbands.repository.MusicalGenreRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * MusicalGenre service.
 * 
 * @author juan.gonzalez.fernandez.jgf
 *
 */
@Service("nusicalGenreService")
@Transactional()
@Slf4j()
public final class MusicalGenreService extends BaseAdminService<MusicalGenre> {

	@Autowired()
	private MusicalGenreRepository genreRepository;
}
