package org.sylrsykssoft.musbands.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.sylrsykssoft.musbands.domain.Album;
import org.sylrsykssoft.musbands.repository.AlbumRepository;

import lombok.extern.slf4j.Slf4j;

@Service("albumService")
@Transactional()
@Slf4j()
public final class AlbumService extends BaseService<Album> {

	@Autowired()
	private AlbumRepository albumRepository;
	
	public List<Album> findByName(final String name) {
		LOGGER.info("AlbumService:findByName Find album by name {}", name);
		return albumRepository.findByName(name);
	}
	
	
}
