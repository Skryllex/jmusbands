package org.sylrsykssoft.musbands.service;

import org.sylrsykssoft.musbands.domain.BaseAdmin;

public interface AdminService<T extends BaseAdmin> extends Service<T, Long> {

	/**
	 * Find by name.
	 * 
	 * @param name Value of the attribute name
	 * 
	 * @return T entity.
	 */
	T findByName(final String name);
}
